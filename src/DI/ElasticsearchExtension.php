<?php

declare(strict_types=1);

/**
 * @package     Haxo
 */

namespace Haxo\Elasticsearch\DI;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Monolog\Logger;
use Nette\DI\CompilerExtension;
use Nette\DI\Helpers;

class ElasticsearchExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $defaults = [
		'hosts' => [] // e.g.: ['127.0.0.1:9200']
	];


	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$config = $this->validateConfig($this->defaults, $this->config);

		$builder->addDefinition($this->prefix('elasticsearch.clientFactory'))
			->setClass(ClientBuilder::class)
			->addSetup('setHosts', [array_filter($config['hosts'])]);

		$builder->addDefinition($this->prefix('elasticsearch.client'))
			->setClass(Client::class)
			->setFactory($this->prefix('@elasticsearch.clientFactory::build'));
	}

}
